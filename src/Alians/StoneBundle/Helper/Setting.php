<?php

namespace Alians\StoneBundle\Helper;

class Setting
{
    const BASE_PATH = 'c:/www3/alians/';
    const BASE_URL = 'http://alians-stone.loc/';
    const PUBLIC_DIR = 'web';
    const UPLOAD_DIR = 'upload';
    const MAXAGE = 6;
    const EXPIRES = '+6 seconds';
    const ENCODE_DEST = 'utf-8';
    const ENCODE_SOURCE = 'windows-1251';
    const BUNDLE = "AliansStoneBundle";
    const SUBSCRIBE_INTERVAL = 60;
    const ORDER_INTERVAL = 60;
   
}//end class